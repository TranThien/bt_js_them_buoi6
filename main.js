document.querySelector("#btn").onclick = function() {
    var numberValue = document.querySelector("#number").value * 1;

    output = "";

    for (var i = 2; i <= numberValue; i++) {
        // Kiểm tra từng số
        // Kiểm tra số đó có phải là số nguyên tố hay không
        var check = true;
        for (var j = 2; j <= Math.sqrt(i); j++) {
            if (i % j === 0) {
                check = false;
                break;
            }
        }
        if (check) {
            output += i + " ";
        }
    }
    document.querySelector("#result").innerHTML = output;
    document.querySelector("#result").style.backgroundColor = "#97dc97";
    document.querySelector("#result").style.color = "red";
};